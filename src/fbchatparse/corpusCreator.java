/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbchatparse;
import java.io.File;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
 * @author swagginwagon
 */



public class corpusCreator {
    /**
    * Creates a corpus with a single author. Mainly for testing purposes.
    * @param author_name the name of the directory 
    * @param folder_name the name of the folder
    */
    public void Folders_verbose(String author_name, String folder_name){
        String main = author_name;
        File auth = new File(main);
        boolean successful = auth.mkdir();
        if (successful){
            System.out.println("Directory creation of "+main+" successful");
        }
        else{
            System.out.println("Directory creation of "+main+" failed");
        }
        
    }
    /**
     * This method accepts two inputs, the author_names and the folder_name which to place the authors in.
     * The folder_name represents the name of the corpus.
     * Currently all files are placed in ignore, that will be changed later.
     * 
     * @param author_names the array which contains all authors from the given FB archive
     * @param folder_name the name of the corpus
     * 
     * 
    */
    public void Folders_verbose(String[] author_names, String folder_name){
        //Currently the output of this method is set to the folder ignore in root. Eventually the name should be changed to output or 
        //something similar.
        
        //String[] main = new String[author_names.length];
        File checkIgnore = new File("ignore");
        if (!checkIgnore.isDirectory()){
            if (checkIgnore.mkdir()){
                System.out.println("ignore directory created.");
            }
            else{
                System.out.println("Creation of ignore failed.");
            }
        }
        for (int i = 0; i < author_names.length; i++){
            File auth = new File("ignore/"+""+folder_name+""+"/"+Array.get(author_names, i)+"/");
            boolean successful = auth.mkdirs();
            if (successful){
                System.out.println("Directory creation of "+Array.get(author_names, i)+" successful");
            }
            else {
                System.out.println("Directory creation of "+Array.get(author_names, i)+" failed");
            }
            
        }
        System.out.println("Directory creation complete.");
    }
    
}
